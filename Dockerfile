ARG FLUENTD_VERSION=1.15
FROM fluent/fluentd:v${FLUENTD_VERSION}

USER root

ARG ES_VERSION=7.15.0
RUN apk add --no-cache --update --virtual .build-deps sudo build-base ruby-dev \
    && fluent-gem install fluent-plugin-prometheus \
    && gem install elasticsearch -v ${ES_VERSION} \
    && gem install fluent-plugin-elasticsearch \
    && gem install fluent-plugin-concat \
    && gem sources --clear-all \
    && apk del .build-deps \
    && rm -rf /home/fluent/.gem/ruby/2.5.0/cache/*.gem

USER fluent
